package at.fhv.itm19.st.web.rest;

import at.fhv.itm19.st.ClubAltachApp;
import at.fhv.itm19.st.domain.MatchResult;
import at.fhv.itm19.st.repository.MatchResultRepository;
import at.fhv.itm19.st.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static at.fhv.itm19.st.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MatchResultResource} REST controller.
 */
@SpringBootTest(classes = ClubAltachApp.class)
public class MatchResultResourceIT {

    private static final String DEFAULT_OPPONENT = "AAAAAAAAAA";
    private static final String UPDATED_OPPONENT = "BBBBBBBBBB";

    private static final String DEFAULT_SCORE = "AAAAAAAAAA";
    private static final String UPDATED_SCORE = "BBBBBBBBBB";

    @Autowired
    private MatchResultRepository matchResultRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMatchResultMockMvc;

    private MatchResult matchResult;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MatchResultResource matchResultResource = new MatchResultResource(matchResultRepository);
        this.restMatchResultMockMvc = MockMvcBuilders.standaloneSetup(matchResultResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MatchResult createEntity(EntityManager em) {
        MatchResult matchResult = new MatchResult()
            .opponent(DEFAULT_OPPONENT)
            .score(DEFAULT_SCORE);
        return matchResult;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MatchResult createUpdatedEntity(EntityManager em) {
        MatchResult matchResult = new MatchResult()
            .opponent(UPDATED_OPPONENT)
            .score(UPDATED_SCORE);
        return matchResult;
    }

    @BeforeEach
    public void initTest() {
        matchResult = createEntity(em);
    }

    @Test
    @Transactional
    public void createMatchResult() throws Exception {
        int databaseSizeBeforeCreate = matchResultRepository.findAll().size();

        // Create the MatchResult
        restMatchResultMockMvc.perform(post("/api/match-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(matchResult)))
            .andExpect(status().isCreated());

        // Validate the MatchResult in the database
        List<MatchResult> matchResultList = matchResultRepository.findAll();
        assertThat(matchResultList).hasSize(databaseSizeBeforeCreate + 1);
        MatchResult testMatchResult = matchResultList.get(matchResultList.size() - 1);
        assertThat(testMatchResult.getOpponent()).isEqualTo(DEFAULT_OPPONENT);
        assertThat(testMatchResult.getScore()).isEqualTo(DEFAULT_SCORE);
    }

    @Test
    @Transactional
    public void createMatchResultWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = matchResultRepository.findAll().size();

        // Create the MatchResult with an existing ID
        matchResult.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMatchResultMockMvc.perform(post("/api/match-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(matchResult)))
            .andExpect(status().isBadRequest());

        // Validate the MatchResult in the database
        List<MatchResult> matchResultList = matchResultRepository.findAll();
        assertThat(matchResultList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkOpponentIsRequired() throws Exception {
        int databaseSizeBeforeTest = matchResultRepository.findAll().size();
        // set the field null
        matchResult.setOpponent(null);

        // Create the MatchResult, which fails.

        restMatchResultMockMvc.perform(post("/api/match-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(matchResult)))
            .andExpect(status().isBadRequest());

        List<MatchResult> matchResultList = matchResultRepository.findAll();
        assertThat(matchResultList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkScoreIsRequired() throws Exception {
        int databaseSizeBeforeTest = matchResultRepository.findAll().size();
        // set the field null
        matchResult.setScore(null);

        // Create the MatchResult, which fails.

        restMatchResultMockMvc.perform(post("/api/match-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(matchResult)))
            .andExpect(status().isBadRequest());

        List<MatchResult> matchResultList = matchResultRepository.findAll();
        assertThat(matchResultList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMatchResults() throws Exception {
        // Initialize the database
        matchResultRepository.saveAndFlush(matchResult);

        // Get all the matchResultList
        restMatchResultMockMvc.perform(get("/api/match-results?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(matchResult.getId().intValue())))
            .andExpect(jsonPath("$.[*].opponent").value(hasItem(DEFAULT_OPPONENT)))
            .andExpect(jsonPath("$.[*].score").value(hasItem(DEFAULT_SCORE)));
    }
    
    @Test
    @Transactional
    public void getMatchResult() throws Exception {
        // Initialize the database
        matchResultRepository.saveAndFlush(matchResult);

        // Get the matchResult
        restMatchResultMockMvc.perform(get("/api/match-results/{id}", matchResult.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(matchResult.getId().intValue()))
            .andExpect(jsonPath("$.opponent").value(DEFAULT_OPPONENT))
            .andExpect(jsonPath("$.score").value(DEFAULT_SCORE));
    }

    @Test
    @Transactional
    public void getNonExistingMatchResult() throws Exception {
        // Get the matchResult
        restMatchResultMockMvc.perform(get("/api/match-results/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMatchResult() throws Exception {
        // Initialize the database
        matchResultRepository.saveAndFlush(matchResult);

        int databaseSizeBeforeUpdate = matchResultRepository.findAll().size();

        // Update the matchResult
        MatchResult updatedMatchResult = matchResultRepository.findById(matchResult.getId()).get();
        // Disconnect from session so that the updates on updatedMatchResult are not directly saved in db
        em.detach(updatedMatchResult);
        updatedMatchResult
            .opponent(UPDATED_OPPONENT)
            .score(UPDATED_SCORE);

        restMatchResultMockMvc.perform(put("/api/match-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMatchResult)))
            .andExpect(status().isOk());

        // Validate the MatchResult in the database
        List<MatchResult> matchResultList = matchResultRepository.findAll();
        assertThat(matchResultList).hasSize(databaseSizeBeforeUpdate);
        MatchResult testMatchResult = matchResultList.get(matchResultList.size() - 1);
        assertThat(testMatchResult.getOpponent()).isEqualTo(UPDATED_OPPONENT);
        assertThat(testMatchResult.getScore()).isEqualTo(UPDATED_SCORE);
    }

    @Test
    @Transactional
    public void updateNonExistingMatchResult() throws Exception {
        int databaseSizeBeforeUpdate = matchResultRepository.findAll().size();

        // Create the MatchResult

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMatchResultMockMvc.perform(put("/api/match-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(matchResult)))
            .andExpect(status().isBadRequest());

        // Validate the MatchResult in the database
        List<MatchResult> matchResultList = matchResultRepository.findAll();
        assertThat(matchResultList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMatchResult() throws Exception {
        // Initialize the database
        matchResultRepository.saveAndFlush(matchResult);

        int databaseSizeBeforeDelete = matchResultRepository.findAll().size();

        // Delete the matchResult
        restMatchResultMockMvc.perform(delete("/api/match-results/{id}", matchResult.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MatchResult> matchResultList = matchResultRepository.findAll();
        assertThat(matchResultList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
