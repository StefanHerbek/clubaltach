import { ITeam } from 'app/shared/model/team.model';

export interface IClub {
  id?: number;
  vereinsName?: string;
  teams?: ITeam[];
}

export const defaultValue: Readonly<IClub> = {};
