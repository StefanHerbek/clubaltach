import { ITeam } from 'app/shared/model/team.model';

export interface IPlayer {
  id?: number;
  name?: string;
  position?: string;
  shirtnumber?: number;
  team?: ITeam;
}

export const defaultValue: Readonly<IPlayer> = {};
