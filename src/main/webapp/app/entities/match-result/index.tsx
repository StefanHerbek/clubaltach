import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import MatchResult from './match-result';
import MatchResultDetail from './match-result-detail';
import MatchResultUpdate from './match-result-update';
import MatchResultDeleteDialog from './match-result-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={MatchResultDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={MatchResultUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={MatchResultUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={MatchResultDetail} />
      <ErrorBoundaryRoute path={match.url} component={MatchResult} />
    </Switch>
  </>
);

export default Routes;
