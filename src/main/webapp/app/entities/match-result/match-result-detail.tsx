import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './match-result.reducer';
import { IMatchResult } from 'app/shared/model/match-result.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IMatchResultDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const MatchResultDetail = (props: IMatchResultDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { matchResultEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="clubAltachApp.matchResult.detail.title">MatchResult</Translate> [<b>{matchResultEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="opponent">
              <Translate contentKey="clubAltachApp.matchResult.opponent">Opponent</Translate>
            </span>
          </dt>
          <dd>{matchResultEntity.opponent}</dd>
          <dt>
            <span id="score">
              <Translate contentKey="clubAltachApp.matchResult.score">Score</Translate>
            </span>
          </dt>
          <dd>{matchResultEntity.score}</dd>
          <dt>
            <Translate contentKey="clubAltachApp.matchResult.team">Team</Translate>
          </dt>
          <dd>{matchResultEntity.team ? matchResultEntity.team.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/match-result" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/match-result/${matchResultEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ matchResult }: IRootState) => ({
  matchResultEntity: matchResult.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(MatchResultDetail);
