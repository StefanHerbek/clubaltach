package at.fhv.itm19.st.repository;

import at.fhv.itm19.st.domain.MatchResult;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MatchResult entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MatchResultRepository extends JpaRepository<MatchResult, Long> {

}
