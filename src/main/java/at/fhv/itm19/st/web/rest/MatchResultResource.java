package at.fhv.itm19.st.web.rest;

import at.fhv.itm19.st.domain.MatchResult;
import at.fhv.itm19.st.repository.MatchResultRepository;
import at.fhv.itm19.st.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link at.fhv.itm19.st.domain.MatchResult}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class MatchResultResource {

    private final Logger log = LoggerFactory.getLogger(MatchResultResource.class);

    private static final String ENTITY_NAME = "matchResult";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MatchResultRepository matchResultRepository;

    public MatchResultResource(MatchResultRepository matchResultRepository) {
        this.matchResultRepository = matchResultRepository;
    }

    /**
     * {@code POST  /match-results} : Create a new matchResult.
     *
     * @param matchResult the matchResult to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new matchResult, or with status {@code 400 (Bad Request)} if the matchResult has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/match-results")
    public ResponseEntity<MatchResult> createMatchResult(@Valid @RequestBody MatchResult matchResult) throws URISyntaxException {
        log.debug("REST request to save MatchResult : {}", matchResult);
        if (matchResult.getId() != null) {
            throw new BadRequestAlertException("A new matchResult cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MatchResult result = matchResultRepository.save(matchResult);
        return ResponseEntity.created(new URI("/api/match-results/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /match-results} : Updates an existing matchResult.
     *
     * @param matchResult the matchResult to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated matchResult,
     * or with status {@code 400 (Bad Request)} if the matchResult is not valid,
     * or with status {@code 500 (Internal Server Error)} if the matchResult couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/match-results")
    public ResponseEntity<MatchResult> updateMatchResult(@Valid @RequestBody MatchResult matchResult) throws URISyntaxException {
        log.debug("REST request to update MatchResult : {}", matchResult);
        if (matchResult.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MatchResult result = matchResultRepository.save(matchResult);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, matchResult.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /match-results} : get all the matchResults.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of matchResults in body.
     */
    @GetMapping("/match-results")
    public ResponseEntity<List<MatchResult>> getAllMatchResults(Pageable pageable) {
        log.debug("REST request to get a page of MatchResults");
        Page<MatchResult> page = matchResultRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /match-results/:id} : get the "id" matchResult.
     *
     * @param id the id of the matchResult to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the matchResult, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/match-results/{id}")
    public ResponseEntity<MatchResult> getMatchResult(@PathVariable Long id) {
        log.debug("REST request to get MatchResult : {}", id);
        Optional<MatchResult> matchResult = matchResultRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(matchResult);
    }

    /**
     * {@code DELETE  /match-results/:id} : delete the "id" matchResult.
     *
     * @param id the id of the matchResult to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/match-results/{id}")
    public ResponseEntity<Void> deleteMatchResult(@PathVariable Long id) {
        log.debug("REST request to delete MatchResult : {}", id);
        matchResultRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
