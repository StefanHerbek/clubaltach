/**
 * View Models used by Spring MVC REST controllers.
 */
package at.fhv.itm19.st.web.rest.vm;
